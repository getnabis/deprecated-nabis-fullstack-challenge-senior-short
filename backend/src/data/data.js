import faker from "faker";

const Posts = (count => {
  const arr = [];

  for (let i = 1; i <= count; i++) {
    arr.push({
      id: i,
      title: faker.company.catchPhrase(),
      authorId: Math.floor(Math.random() * (3 - 1) + 1),
      votes: Math.floor(Math.random() * (5000 - 20) + 20)
    });
  }

  return arr;
})(1000);

// [
//   ({
//     id: 1,
//     title: "GraphQL Rocks!",
//     authorId: 1,
//     votes: 100
//   },
//   {
//     id: 2,
//     title: "Warriors Win! Happy!",
//     authorId: 2,
//     votes: 99
//   },
//   {
//     id: 3,
//     title: "Warriors Lose! SAD!",
//     authorId: 3,
//     votes: 9001
//   },
//   {
//     id: 4,
//     title: "Email security and why you should care",
//     authorId: 3,
//     votes: 7
//   })
// ];

const Authors = [
  {
    id: 1,
    firstName: "Lee",
    lastName: "Byron"
  },
  {
    id: 2,
    firstName: "Steph",
    lastName: "Curry"
  },
  {
    id: 3,
    firstName: "Hillary",
    lastName: "Clinton"
  }
];

export { Posts, Authors };
